# react-template #

A basic setup for React front end development. Note that this is intended for projects not requiring a server. 


### How do I get set up? ###

* run `npm install`
* run `gulp` to start watching js and css files
* run `npm run dev` to start up `lite-server` to handle development
