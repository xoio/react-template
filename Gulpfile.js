var watchify = require('watchify');
var browserify = require('browserify');
var gulp = require('gulp');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var sourcemaps = require('gulp-sourcemaps');
var babelify = require('babelify');
var livereactload = require("livereactload");

//////////////////////////////////////////
// add custom browserify options here
var customOpts = {
    entries: ['./src/main.js'],
    debug: true,
    transform:babelify
};
var opts = Object.assign({}, watchify.args, customOpts);
var bundler = watchify(browserify(opts));
bundler.plugin(livereactload);

gulp.task('js',function(){

    return bundler.bundle()
        // log errors if they happen
        .on('error', function(e){
            console.log(e)
        })
        .pipe(source('bundle.js'))
        // optional, remove if you don't need to buffer file contents
        .pipe(buffer())
        // optional, remove if you dont want sourcemaps
        .pipe(sourcemaps.init({loadMaps: true})) // loads map from browserify file
        // Add transformation tasks to the pipeline here.
        .pipe(sourcemaps.write('./')) // writes .map file
        .pipe(gulp.dest('./public/js'));
});

////////////////////////////////////////////
gulp.task('css', function () {
    var postcss    = require('gulp-postcss');
    return gulp.src('./styles/main.css')
        .pipe( sourcemaps.init() )
        .pipe( postcss([ require('precss') ]) )
        .pipe( sourcemaps.write('.') )
        .pipe( gulp.dest('./public/css') );
});
//////////////////////////////////////////////
gulp.task("default",["js","css"],function(){
    gulp.watch(["./styles/.css"],["css"]);
    gulp.watch(["./styles/**/*.scss"],["css"]);
    gulp.watch(["./src/**/*.js"],["js"]);
});